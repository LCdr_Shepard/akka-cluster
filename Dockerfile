FROM frolvlad/alpine-oraclejdk8

ENTRYPOINT ["java" ,"-jar", "/app/app.jar"]

ADD ./target/scala-2.12/Cluster-assembly*.jar /app/app.jar
