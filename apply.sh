#!/bin/bash
kubectl apply -f kubernetes/seed
sleep 4
kubectl apply -f kubernetes/workers

kubectl get pods -o wide
watch kubectl get pods -o wide
