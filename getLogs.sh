#!/bin/bash

podname=$1
f1=$2
f2=$3
f3=$4
f4=$5

kubectl logs $podname > log.txt
kubectl logs $f1 > f1.txt
kubectl logs $f2 > f2.txt
kubectl logs $f3 > f3.txt
kubectl logs $f4 > f4.txt

vim log.txt -c "%s/\[.*\]//g|%s/org.*ath//g|%s/at.*\d//g|%s/no.*ath//g|%s/hea.*lis//g|%s/[^0-9,.]//g|x"

#vim f1.txt -c "1,\$-1d|x"
#vim f2.txt -c "1,\$-1d|x"
#vim f3.txt -c "1,\$-1d|x"
#vim f4.txt -c "1,\$-1d|x"

#cat f1.txt >> log.txt
#cat f2.txt >> log.txt
#cat f3.txt >> log.txt
#cat f4.txt >> log.txt


