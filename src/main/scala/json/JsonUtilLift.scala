package json

import it.unibo.scafi.incarnations.BasicSimulationIncarnation
import it.unibo.scafi.incarnations.BasicSimulationIncarnation.{Rep, factory, _}
import net.liftweb.json.Extraction.decompose
import net.liftweb.json.JsonAST.JField
import net.liftweb.json._
import net.liftweb.json.Serialization.write

class JsonUtilLift {

  implicit val formats: DefaultFormats.type = net.liftweb.json.DefaultFormats

  def getJsonExport(export: ExportImpl): JValue = {
    val exportMap = export.getMap

    var list: List[Map[String, String]] = List()

    for ((p, v) <- exportMap) {
      var map: Map[String, String] = Map("path" -> p.toString.replace("P:/", ""))

      v match {
        case v: Double if v.isInfinity =>
          map += ("value" -> "Inf")
        case _ =>
          map += ("value" -> v.toString)

      }

      list = list ++ List(map)
    }

    decompose(list)

  }

  def getJsonExportString(export: ExportImpl): String = write(getJsonExport(export))

  def formatResult(result: String): Any = {
    if (result.equals("Inf")) {
      Double.PositiveInfinity
    } else if (scala.util.Try(result.toInt).isSuccess) {
      result.toInt
    } else if (scala.util.Try(result.toDouble).isSuccess) {
      result.toDouble
    }else if (scala.util.Try(result.toBoolean).isSuccess) {
      result.toBoolean
    } else {
      if (!result.toString.equals("()")) {
        val plessString = result.replace("(", "").replace(")", "")
        val tupleValues: Array[String] = plessString.split(",")
        if(tupleValues.length == 2) {
          (formatResult(tupleValues(0)), formatResult(tupleValues(1)))
        } else if (tupleValues.length == 3){
          (formatResult(tupleValues(0)), formatResult(tupleValues(1)), formatResult(tupleValues(2)))
        } else {
          //TODO
        }
      } else {
        result
      }
    }
  }

  def createPath(string: String): Path = {
    var path: Path = factory.emptyPath()
    val pathStrings: Array[String] = string.split("/")
    for (stringElem <- pathStrings) {
      if (stringElem.length > 0) {
        val index: String = stringElem.substring(stringElem.length - 2, stringElem.length - 1)
        val tempString = stringElem.substring(0, stringElem.length - 3).trim
        tempString match {
          case "Rep" =>
            path = path.push(Rep[Any](index.toInt))
          case "Nbr" =>
            path = path.push(Nbr[Any](index.toInt))
          case "FoldHood" =>
            path = path.push(FoldHood[Any](index.toInt))
        }
      }
    }
    path
  }

  def createExport(input: String): BasicSimulationIncarnation.EXPORT = {
    val export: BasicSimulationIncarnation.EXPORT = factory.emptyExport()

    val json = parse(input)

    json.extract[List[JObject]].foreach(elem => {
      var result: String = null
      var path: Path = null
      elem.extract[JObject].obj.foreach {
        case field: JField if field.name.equals("path") =>
          path = createPath(field.value.extract[String])
        case field: JField if field.name.equals("value") =>
          result = field.value.extract[String]
      }

      if (result != null && path != null)
        export.put(path, formatResult(result))
    })

    export
  }

}
