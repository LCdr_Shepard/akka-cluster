package json

import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import it.unibo.scafi.incarnations.BasicSimulationIncarnation.{Rep, factory}
import it.unibo.scafi.incarnations.BasicSimulationIncarnation
import scala.util.parsing.json.{JSONArray, JSONObject}

class JsonUtil {
  
  def getJsonExport(export: ExportImpl): JSONArray = {
    val exportMap = export.getMap
    var list: List[JSONObject] = List()
    for ((p, v) <- exportMap){
      var map: Map[String, String] = Map("path" -> p.toString.replace("P:/", ""))

      v match {
        case v: Double if v.isInfinity =>
          map += ("result" -> "Inf")
        case _ =>
          map += ("result" -> v.toString)

      }
      //list ++ List(JSONObject(map))
      list = list ++ List(JSONObject(map))
    }

    JSONArray(list)
  }

  def getJsonExportString(export: ExportImpl): String = getJsonExport(export).toString()

  def formatResult(result: String): Any = {
    if (result.equals("Inf")) {
      Double.PositiveInfinity
    } else if (scala.util.Try(result.toInt).isSuccess) {
      result.toInt
    } else if (scala.util.Try(result.toDouble).isSuccess) {
      result.toDouble
    }else if (scala.util.Try(result.toBoolean).isSuccess) {
      result.toBoolean
    } else {
      if (!result.toString.equals("()")) {
        val plessString = result.replace("(", "").replace(")", "")
        val tupleValues: Array[String] = plessString.split(",")
        if(tupleValues.length == 2) {
          (formatResult(tupleValues(0)), formatResult(tupleValues(1)))
        } else if (tupleValues.length == 3){
          (formatResult(tupleValues(0)), formatResult(tupleValues(1)), formatResult(tupleValues(2)))
        } else {
          //TODO
        }
      } else {
        result
      }
    }
  }

  def createPath(string: String): Path = {
    var path: Path = factory.emptyPath()
    val pathStrings: Array[String] = string.split("/")
    for (stringElem <- pathStrings) {
      if (stringElem.length > 0) {
        val index: String = stringElem.substring(stringElem.length - 2, stringElem.length - 1)
        val tempString = stringElem.substring(0, stringElem.length - 3).trim
        tempString match {
          case "Rep" =>
            path = path.push(Rep[Any](index.toInt))
          case "Nbr" =>
            path = path.push(Nbr[Any](index.toInt))
          case "FoldHood" =>
            path = path.push(FoldHood[Any](index.toInt))
        }
      }
    }
    path
  }

  def createExport(jSONArray: JSONArray): BasicSimulationIncarnation.EXPORT = {
    val export: BasicSimulationIncarnation.EXPORT = factory.emptyExport()

    jSONArray.list.asInstanceOf[List[JSONObject]].foreach(item => {
      val map = item.obj
      val result = map("result")
      val path: Path = createPath(map("path").toString)

      export.put(path, formatResult(result.toString))
    })

    export
  }

}
