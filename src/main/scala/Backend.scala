import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import it.unibo.scafi.incarnations.BasicSimulationIncarnation.{AggregateProgram, BlockG, factory}
import language.postfixOps
import akka.actor.{Actor, ActorLogging, ActorSystem, Address, Props}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent.MemberUp
import akka.stream.ActorMaterializer
import com.redis.{GeoRadiusMember, RedisClient}
import com.typesafe.config.ConfigFactory
import it.unibo.scafi.incarnations.BasicSimulationIncarnation
import json.JsonUtilLift
import model.FrontendMessage


class BasicProgram extends AggregateProgram with BlockG with StandardSensors {
  override def main(): Double = distanceTo(source)

  def source: Boolean = sense[Boolean]("source")
}

class Backend extends Actor with ActorLogging {

  val REDIS_PORT = 6379
  val TWEMPROXY_PORT = 22121
  val cluster = Cluster(context.system)
  val jsonUtil = new JsonUtilLift

  var count = 0
  var countStart: Long = System.currentTimeMillis()
  /*var t1: ListBuffer[Long] = ListBuffer()
  var t2: ListBuffer[Long] = ListBuffer()
  var t3: ListBuffer[Long] = ListBuffer()*/

  override def preStart(): Unit = cluster.subscribe(self, classOf[MemberUp])

  override def postStop(): Unit = cluster.unsubscribe(self)

  def getSpatialClient: RedisClient = new RedisClient("redis01", REDIS_PORT)

  def getClient: RedisClient = new RedisClient("twemproxy", TWEMPROXY_PORT)

  def getNeighbors(id: Int, pos: (Double, Double)): List[Option[GeoRadiusMember]] = {
    getSpatialClient.geoadd("Space", Seq((pos._1, pos._2, id)))
    getSpatialClient.georadius("Space", pos._1, pos._2, 200, "km", withCoord = false, withDist = true, withHash = false, None, None, None, None).get
  }

  def getExports(id: Int, n: List[Option[GeoRadiusMember]]): Map[BasicSimulationIncarnation.ID, BasicSimulationIncarnation.EXPORT] = {
    var exports: Map[BasicSimulationIncarnation.ID, BasicSimulationIncarnation.EXPORT] = Map()

    val t1 = System.currentTimeMillis()

    val idList = n.collect { case e: Option[GeoRadiusMember] if e.get.member.get.toInt != id => e.get.member.get.toInt }
    //val idList: List[Int] = n.map(e => e.get.member.get.toInt).filter(e => if(id != e) true else false)

    val first = idList.drop(1)

    val t2 = System.currentTimeMillis()

    val get = getClient.mget(first, idList:_*)//.getOrElse(List()).map(e => e.getOrElse(""))

    val t3 = System.currentTimeMillis()

    val res: List[(Int, String)] = idList.zip(get.getOrElse(List()).map(e => e.getOrElse("")))

    val t4 = System.currentTimeMillis()
    var t5,t7: Long= 0
    res.par.foreach(value => {
      if (!value._2.equals("")) {
        t5 = System.currentTimeMillis()
        exports += (value._1 -> jsonUtil.createExport(value._2))
        t7 = System.currentTimeMillis()
      } else {
        exports += (value._1 -> factory.emptyExport())
      }
    })

    val t8 = System.currentTimeMillis()

    log.info(" " + (t7-t5))
    log.info((t2-t1) + " | " +(t3-t2) + " | " +(t4-t3) + " | " +(t8-t4))

    exports
  }


  def getNbrRange(id: Int, n: List[Option[GeoRadiusMember]]): Map[BasicSimulationIncarnation.ID, Double] = {
    var range: Map[BasicSimulationIncarnation.ID, Double] = Map()
    n.foreach(item => {
      range += (item.get.member.get.toInt -> item.get.dist.get.toDouble)
    })
    range += id -> 0
    range
  }


  def receive: PartialFunction[Any, Unit] = {
    case msg: FrontendMessage =>
      val id = msg.id
      val pos = (msg.x, msg.y)
      val source = msg.source

      val start = System.currentTimeMillis()
      val neighbors = getNeighbors(id, pos)

      val t1 = System.currentTimeMillis()

      val exports = getExports(id, neighbors)

      val t2 = System.currentTimeMillis()

      val nbrRange = getNbrRange(id, neighbors)

      val t3 = System.currentTimeMillis()

      val ctx = factory.context(selfId = id,
        exports,
        lsens = Map("POSITION" -> (pos._1, pos._2), "source" -> source),
        nbsens = Map("nbrRange" -> nbrRange))
      val c = new BasicProgram()


      val currentDeviceExport: String = jsonUtil.getJsonExportString(c.round(ctx).asInstanceOf[ExportImpl])
      //log.info(currentDeviceExport)
      val t4 = System.currentTimeMillis()
      count+=1
      getClient.set(id, currentDeviceExport)

      if(id == 2000){
        log.info("Count: " + count + " in " + (System.currentTimeMillis() - countStart) + " ms" )
        count = 0
        countStart = System.currentTimeMillis()
      }

      log.info("tot time: " + (t4 - start) + " | aggregate time: " + (t4-t3) + " | nbrdist time: " + (t3-t2) + " | export time: " + (t2-t1)+ " | n time: " + (t1-start))
      /*if(id == 2000){
        println("tested")
        sender() ! BackendMessage("mockRes")
      } else {
        sender() ! BackendMessage(currentDeviceExport)
      }*/
    /*case state: CurrentClusterState =>
      state.members.filter(_.status == MemberStatus.Up) foreach register
    case MemberUp(m) => register(m)*/
  }

  /*def register(member: Member): Unit =
    if (member.hasRole("frontend")) {
      context.actorSelection(RootActorPath(member.address) / "user" / "frontend") ! BackendRegistration
    }}*/
}

object Backend {

  private val CLUSTER_NAME: String = "ActorSystem"

  def main(args: Array[String]): Unit = {

    val config = ConfigFactory.parseString("akka.cluster.roles = [backend]").
      withFallback(ConfigFactory.load())

    val system = ActorSystem(CLUSTER_NAME, config)
    system.actorOf(Props[Backend], name = "backend")

    ActorMaterializer.create(system)
    val cluster = Cluster.get(system)

    val addresses = System.getenv().get("SEED_NODES").split(",")
      .map(ip => new Address("akka.tcp", CLUSTER_NAME, ip, 2551))
      .toList

    cluster.joinSeedNodes(addresses)
  }
}