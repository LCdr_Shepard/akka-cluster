import akka.cluster.metrics.ClusterMetricsChanged
import akka.cluster.ClusterEvent.CurrentClusterState
import scala.collection.immutable.TreeMap
import akka.cluster.metrics.NodeMetrics
import akka.cluster.metrics.StandardMetrics.HeapMemory
import akka.cluster.metrics.StandardMetrics.Cpu
import akka.cluster.metrics.ClusterMetricsExtension
import language.postfixOps
import scala.concurrent.duration._
import akka.actor.{Actor, ActorLogging, ActorSystem, Address, Props}
import akka.cluster.Cluster
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory

class MetricsListener extends Actor with ActorLogging {
  //val selfAddress = Cluster(context.system).selfAddress
  val cluster = Cluster(context.system)
  val extension = ClusterMetricsExtension(context.system)
  //var metricsMap: Map[String, (Double, (Double, Int))] = Map()
  private[this] var heapMap: TreeMap[Address, Seq[Long]] = TreeMap.empty
  private[this] var loadMap: TreeMap[Address, Seq[Double]] = TreeMap.empty
  var maxHeap: Long = 0
  //private var backends: Set[Address] = HashSet.empty

  import context.dispatcher
  context.system.scheduler.schedule(20.seconds, 20.seconds, self, "logMetricsFormat")


  // Subscribe unto ClusterMetricsEvent events.
  override def preStart(): Unit = {
    extension.subscribe(self)
    //cluster.subscribe(self, classOf[MemberUp])
  }
  
  // Unsubscribe from ClusterMetricsEvent events.
  override def postStop(): Unit = {
    extension.unsubscribe(self)
    //Cluster(context.system).unsubscribe(self)
  }

  def receive: PartialFunction[Any, Unit] = {
    /*case MemberUp(m) if m.roles.contains("backend") =>
      backends += m.address*/
    case ClusterMetricsChanged(clusterMetrics) =>
      clusterMetrics/*.filter(item => backends.contains(item.address))*/ foreach { nodeMetrics =>
        updateHeapMap(nodeMetrics)
        updateLoadMap(nodeMetrics)
      }
    case _: CurrentClusterState => // Ignore.
    case "logMetricsFormat" =>
      val (h, l) = calculateAverages
      log.info(System.currentTimeMillis + "," + h.mkString(",") + "," + l.mkString(","))
      heapMap = TreeMap.empty
      loadMap = TreeMap.empty
    case "logMetrics"  =>
      log.info(heapMap.toString + loadMap.toString() + maxHeap)
      heapMap = TreeMap.empty
      loadMap = TreeMap.empty
  }

  def calculateAverages: (Iterable[Long], Iterable[Double]) ={
    val heapAvg = heapMap.values map (nHeapUse => if (nHeapUse.isEmpty) 0 else nHeapUse.sum / nHeapUse.length)
    val loadAvg = loadMap.values map (nLoad => if (nLoad.isEmpty) 0 else nLoad.sum / nLoad.length)
    (heapAvg, loadAvg)
  }

  def updateHeapMap(metrics: NodeMetrics): Unit = metrics match {
    case HeapMemory(address, _, used, _, max) =>
      val updatedHeapUse: Seq[Long] = heapMap.getOrElse(address, Seq.empty) :+ (used.doubleValue / 1024 / 1024).toLong
      heapMap += address -> updatedHeapUse
      maxHeap = max.getOrElse(0)
    case _ => // No heap info.
  }

  def updateLoadMap(metrics: NodeMetrics): Unit = metrics match {
   case Cpu(address, _, Some(systemLoadAverage), _, _, _) =>
      val updatedHeapUse: Seq[Double] = loadMap.getOrElse(address, Seq.empty) :+ systemLoadAverage
      loadMap += address -> updatedHeapUse

    case _ => // No heap info.
  }

}

object Logger {

  private val CLUSTER_NAME: String = "ActorSystem"

  def main(args: Array[String]): Unit = {

    val config = ConfigFactory.parseString("akka.cluster.roles = [logger]").
      withFallback(ConfigFactory.load())

    val system: ActorSystem = ActorSystem(CLUSTER_NAME, config)
    system.actorOf(Props[MetricsListener], name = "logger")

    ActorMaterializer.create(system)
    val cluster = Cluster.get(system)

    val addresses = System.getenv().get("SEED_NODES").split(",")
      .map(ip => new Address("akka.tcp", CLUSTER_NAME, ip, 2551))
      .toList

    cluster.joinSeedNodes(addresses)

    /*system.scheduler.schedule(2.seconds, 100.millisecond) {
      implicit val timeout: Timeout = Timeout(5 seconds)
      logger ? "logMetrics" onComplete {
        println(_)
      }

    }*/
  }
}
