import java.util.concurrent.atomic.AtomicInteger
import language.postfixOps
import scala.concurrent.duration._
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Address, Props}
import akka.cluster.Cluster
import akka.stream.ActorMaterializer
import akka.routing.FromConfig
import com.typesafe.config.ConfigFactory
import model.{BackendMessage, FrontendMessage, MockMessage}
import scala.util.Random

class Frontend extends Actor with ActorLogging {

  var created: Int  = 0
  var sent: Int = 0
  var received: Int = 0
  val backend: ActorRef = context.actorOf(FromConfig.props(), name = "backendRouter")
  val counter = new AtomicInteger
  private val rand = new Random()
  private val oldPos = ((rand.nextDouble() - 0.5)/500,(rand.nextDouble() - 0.5)/500)
  var startTime = 0

  val mockMsg = FrontendMessage(2000)
  var start: Long = 0

  import context.dispatcher
  context.system.scheduler.schedule(2.seconds, 50.millis) {
    if(counter.get() > 1000) counter.set(0)
    created += 1
    val msg = FrontendMessage(counter.getAndIncrement(), oldPos._1, oldPos._2, if(counter.get() == 1) true else false)
    self forward msg
  }

  context.system.scheduler.schedule(10.seconds, 5.seconds) {
    self forward MockMessage("test")
  }

  def receive: PartialFunction[Any, Unit] = {
    case msg: FrontendMessage =>
      sent += 1
      backend ! msg
    case msg: BackendMessage =>
      if(msg.export.equals("mockRes")){
        log.info("Time elapsed for msg: " + (System.currentTimeMillis() - start))
      } else {
        received += 1
        if(received%1000 == 0)
          log.info("Created: {} - Sent: {} - Received: {}", created, sent, received)
      }
    case _ : MockMessage =>
      start = System.currentTimeMillis()
      backend ! mockMsg

  }
}

object Frontend {

  private val CLUSTER_NAME: String = "ActorSystem"

  def main(args: Array[String]): Unit = {

    val config = ConfigFactory.parseString("akka.cluster.roles = [frontend]").
      withFallback(ConfigFactory.load())

    val system = ActorSystem(CLUSTER_NAME, config)
    system.actorOf(Props[Frontend], name = "frontend")

    ActorMaterializer.create(system)
    val cluster = Cluster.get(system)

    val addresses = System.getenv().get("SEED_NODES").split(",")
      .map(ip => new Address("akka.tcp", CLUSTER_NAME, ip, 2551))
      .toList

    cluster.joinSeedNodes(addresses)
  }
}
