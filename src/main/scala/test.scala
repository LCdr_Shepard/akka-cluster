import it.unibo.scafi.incarnations.BasicSimulationIncarnation
import json.JsonUtil

import scala.util.parsing.json.{JSON, JSONArray, JSONObject}
import net.liftweb.json.JsonAST._
import net.liftweb.json.Extraction._
import net.liftweb.json.JObject
import net.liftweb.json._
import net.liftweb.json.Serialization.{read, write}
import it.unibo.scafi.incarnations.BasicSimulationIncarnation._

object test extends App {

  implicit val formats = net.liftweb.json.DefaultFormats

  val jsonUtil = new JsonUtil
  val json = "[{\"path\" : \"Rep(0)\\/FoldHood(0)\\/Nbr(2)\", \"result\" : \"0.0\"}, {\"path\" : \"Rep(0)\", \"result\" : \"(1.7976931348623157E308,2.0E-4)\"}, {\"path\" : \"Rep(0)\\/FoldHood(0)\\/Nbr(1)\", \"result\" : \"1.7976931348623157E308\"}, {\"path\" : \"Rep(0)\\/FoldHood(0)\\/Nbr(0)\", \"result\" : \"1\"}, {\"path\" : \"Rep(0)\\/FoldHood(0)\", \"result\" : \"(1.7976931348623157E308,2.0E-4)\"}, {\"path\" : \"\", \"result\" : \"2.0E-4\"}]"

  val s = JSON.parseRaw(json).get.asInstanceOf[JSONArray]

  val exp: BasicSimulationIncarnation.ExportImpl = jsonUtil.createExport(s).asInstanceOf[BasicSimulationIncarnation.ExportImpl]

  val exp2 = exp.getMap



  var list: List[Map[String, String]] = List()

  for ((p, v) <- exp2) {
    var map: Map[String, String] = Map("path" -> p.toString.replace("P:/", ""))

    v match {
      case v: Double if v.isInfinity =>
        map += ("value" -> "Inf")
      case _ =>
        map += ("value" -> v.toString)

    }

    list = list ++ List(map)
  }

  val res = decompose(list)
  val string: String = write(res)
  val test = parse(string)
  println(string)



  val export: BasicSimulationIncarnation.EXPORT = factory.emptyExport()


  val l = res.extract[List[JObject]]


  l.foreach(elem => {
    val list3 = elem.extract[JObject]

    var result: String = null
    var path: Path = null
    list3.obj.foreach {
      case field: JField if field.name.equals("path") =>
        path = jsonUtil.createPath(field.value.extract[String])
      case field: JField if field.name.equals("value") =>
        result = field.value.extract[String]
    }

    export.put(path, jsonUtil.formatResult(result))
  })

  //println(export)

}