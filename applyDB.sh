#!/bin/bash
kubectl create configmap redis-conf --from-file=kubernetes-twemproxy-master/redis.conf
kubectl create -f kubernetes-twemproxy-master/deployments/pv.yaml
kubectl create -f kubernetes-twemproxy-master/deployments/redis01.yaml
kubectl create -f kubernetes-twemproxy-master/deployments/redis02.yaml
kubectl create -f kubernetes-twemproxy-master/deployments/redis03.yaml
kubectl create -f kubernetes-twemproxy-master/services/redis01.yaml
kubectl create -f kubernetes-twemproxy-master/services/redis02.yaml
kubectl create -f kubernetes-twemproxy-master/services/redis03.yaml
kubectl create configmap twemproxy-conf --from-file=kubernetes-twemproxy-master/twemproxy.yaml
kubectl create -f kubernetes-twemproxy-master/deployments/twemproxy.yaml
kubectl create -f kubernetes-twemproxy-master/services/twemproxy.yaml
